# Sortoff2022

A remake of one of my first projects, sortoff, in which sorting algorithms are visualized and can be run against each other in real time. This time, though, I use raygui instead of painstakingly drawing the display pixel by pixel using WinGDI functions.


raylib and raygui are needed to build this project.


Roadmap:


~~-Visualize static randomly generated array~~

~~-Add basic sorting algorithm~~

~~-Add live visualization framework~~

~~-Visualize basic sorting algorithm~~

~~-Validate sorted array~~

~~-Clean up GUI~~

~~-Add auxillary array display~~

-Add more sorting algorithms

-Add "head to head mode"

-Clean up GUI more

-Add "tournament mode"

-Add more input distributions

-Clean up GUI more
