#pragma once
#ifndef ARRAY_UTILS
#include <random>
#include <cstdlib>
#include <thread>
#include <assert.h>
#include <numeric>
#include "VisualArray.h"
#endif // !ARRAY_UTILS

// if you're adding an algorithm make sure to add it to the enum and list as well as the appropriate positions in Sortoff.cpp

void insertionSort(VisualArray arr);
void MergeSort(VisualArray A);
void quickSortLR(VisualArray A);
void QuickSortDualPivot(VisualArray a);
void CocktailShakerSort(VisualArray A);
void CombSort(VisualArray A);
void ShellSort(VisualArray A);
void HeapSort(VisualArray A);
void RadixSortLSD(VisualArray A);
void OddEvenSort(VisualArray A);
void BitonicSortNetwork(VisualArray A);

enum Algorithm
{
	START_ENUM,
	INSERTION,
	COCKTAIL_SHAKER,
	COMB_SORT,
	SHELL_SORT,
	MERGE,
	QUICKSORT,
	DP_QUICKSORT,
	HEAP_SORT,
	RADIX_LSD,
	ODD_EVEN,
	BITONIC
};