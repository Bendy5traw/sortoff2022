#pragma once
#include <iostream>
#include "raylib.h"
#define RAYGUI_IMPLEMENTATION
#include "raygui.h"
#include "../styles/dark/dark.h"

class windowInfo
{
public:
	int width, height;
};

enum gameTypes
{
	SINGLE,
	HEAD_TO_HEAD,
	TOURNAMENT,
	RUN
};

namespace buttons
{
	bool arrayDisplay;
	gameTypes gameType;
}

void setupArrayView(int numArrays, windowInfo window);
void validateArray(int arrayNum, windowInfo window);