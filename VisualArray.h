#pragma once
#ifndef ARRAY_UTILS
#include <random>
#include <cstdlib>
#include <thread>
#endif // !ARRAY_UTILS

enum Distribution
{
	RANDOM,
	MOSTLY_SORTED,
	END_ENUM
};

class VisualArray
{
public:
	int operator[](int i);
	int hiddenAccess(int i); // used for visualization functions
	void init(int size, Distribution distribution, bool auxArray = false);
	int size;
	bool* access; // used to show which items are currently being accessed in visualization
	bool* mark; // used by the algorithm to mark important elements
	int arrayNum; // there will be multiple VisualArrays at once, this is to keep track of which one to update in display function
	void set(int i, int item);
	void swap(int i, int j);
	void unmarkAll();
	~VisualArray();
private:
	void update();
	int indexOf(int item);
	int* array;
	bool arrayContainsItem(int item);
	bool auxArray;
};

namespace Globals
{
	extern int numArrays;
	extern std::vector<VisualArray> arrays;
	extern bool ready; // this needs to be replaced with a vector to run multiple algos simultaneously
	extern bool finished; // temporary fix to alert algo finished. also needs to be a vector for scaling
	extern std::vector<std::shared_ptr<std::thread>> threads;
	extern int speed;
	extern std::vector<bool> auxArrays;
}