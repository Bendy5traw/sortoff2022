#include "Algorithms.h"

// Many algorithms are from Timo Bingmann's sound-of-sorting
// Insertion sort, merge sort, quicksort LR
void insertionSort(VisualArray arr)
{
    for (int i = 1; i < arr.size; ++i)
    {
        int key = arr[i];
        arr.mark[i] = true;

        int j = i - 1;

        while (j >= 0 && arr[j] > key)
        {
            arr.swap(j, j+1);
            j--;
        }

        arr.mark[i] = false;
    }

    Globals::finished = true;
}

void Merge(VisualArray A, size_t lo, size_t mid, size_t hi)
{
    // mark merge boundaries
    A.mark[lo] = true;
    A.mark[mid] = true;
    A.mark[hi - 1] = true;

    // allocate output
    VisualArray out;
    out.init(A.size, END_ENUM, true);
    Globals::arrays.push_back(out);

    // merge
    size_t i = lo, j = mid, o = 0; // first and second halves
    while (i < mid && j < hi)
    {
        // copy out for fewer time steps
        int ai = A[i], aj = A[j];

        out.set(o++, (ai < aj ? (++i, ai) : (++j, aj)));
    }

    // copy rest
    while (i < mid) out.set(o++, A[i++]);
    while (j < hi) out.set(o++, A[j++]);

    assert(o == hi - lo);

    A.mark[mid] = false;

    // copy back
    for (i = 0; i < hi - lo; ++i)
        A.set(lo + i, out[i]);

    Globals::arrays.erase(Globals::arrays.begin() + 1); // this method needs to be changed for head to head and tournament

    A.mark[lo] = false;
    A.mark[hi - 1] = false;
}

void MergeSort(VisualArray A, size_t lo, size_t hi)
{
    if (lo + 1 < hi)
    {
        size_t mid = (lo + hi) / 2;

        MergeSort(A, lo, mid);
        MergeSort(A, mid, hi);

        Merge(A, lo, mid, hi);
    }
}

void MergeSort(VisualArray A)
{
    MergeSort(A, 0, A.size);
    Globals::finished = true;
}

void quickSortLR(VisualArray A, int lo, int hi)
{
    // pick pivot and watch
    volatile int p = (lo + hi) / 2;

    int pivot = A[p];
    A.mark[p] = true;

    volatile int i = lo, j = hi;
    A.mark[i] = true;
    A.mark[j] = true;

    while (i <= j)
    {
        while (A[i] < pivot)
            i++;

        while (A[j] > pivot)
            j--;

        if (i <= j)
        {
            A.swap(i, j);

            // follow pivot if it is swapped
            if (p == i) p = j;
            else if (p == j) p = i;

            i++, j--;
        }
    }

    A.unmarkAll();

    if (lo < j)
        quickSortLR(A, lo, j);

    if (i < hi)
        quickSortLR(A, i, hi);
}

void quickSortLR(VisualArray A)
{
    quickSortLR(A, 0, A.size - 1);
    Globals::finished = true;
}

// by Sebastian Wild

void dualPivotYaroslavskiy(VisualArray a, int left, int right)
{
    if (right > left)
    {
        if (a[left] > a[right]) {
            a.swap(left, right);
        }

        const int p = a[left];
        const int q = a[right];

        a.mark[left] = true;
        a.mark[right] = true;

        volatile int l = left + 1;
        volatile int g = right - 1;
        volatile int k = l;

        a.mark[l] = true;
        a.mark[g] = true;
        a.mark[k] = true;

        while (k <= g)
        {
            if (a[k] < p) {
                a.swap(k, l);
                ++l;
            }
            else if (a[k] >= q) {
                while (a[g] > q && k < g)  --g;
                a.swap(k, g);
                --g;

                if (a[k] < p) {
                    a.swap(k, l);
                    ++l;
                }
            }
            ++k;
        }
        --l;
        ++g;
        a.swap(left, l);
        a.swap(right, g);

        a.unmarkAll();

        dualPivotYaroslavskiy(a, left, l - 1);
        dualPivotYaroslavskiy(a, l + 1, g - 1);
        dualPivotYaroslavskiy(a, g + 1, right);
    }
}

void QuickSortDualPivot(VisualArray a)
{
    dualPivotYaroslavskiy(a, 0, a.size - 1);
    Globals::finished = true;
}

// from http://de.wikibooks.org/wiki/Algorithmen_und_Datenstrukturen_in_C/_Shakersort

void CocktailShakerSort(VisualArray A)
{
    size_t lo = 0, hi = A.size - 1, mov = lo;

    while (lo < hi)
    {
        for (size_t i = hi; i > lo; --i)
        {
            if (A[i - 1] > A[i])
            {
                A.swap(i - 1, i);
                mov = i;
            }
        }

        lo = mov;

        for (size_t i = lo; i < hi; ++i)
        {
            if (A[i] > A[i + 1])
            {
                A.swap(i, i + 1);
                mov = i;
            }
        }

        hi = mov;
    }

    Globals::finished = true;
}

// from http://en.wikipediA.org/wiki/Comb_sort
void CombSort(VisualArray A)
{
    const double shrink = 1.3;

    bool swapped = false;
    size_t gap = A.size;

    while ((gap > 1) || swapped)
    {
        if (gap > 1) {
            gap = (size_t)((float)gap / shrink);
        }

        swapped = false;

        for (size_t i = 0; gap + i < A.size; ++i)
        {
            if (A[i] > A[i + gap])
            {
                A.swap(i, i + gap);
                swapped = true;
            }
        }
    }

    Globals::finished = true;
}

// with gaps by Robert Sedgewick from http://www.cs.princeton.edu/~rs/shell/shell.c
void ShellSort(VisualArray A)
{
    size_t incs[16] = { 1391376, 463792, 198768, 86961, 33936,
                        13776, 4592, 1968, 861, 336,
                        112, 48, 21, 7, 3, 1 };

    for (size_t k = 0; k < 16; k++)
    {
        for (size_t h = incs[k], i = h; i < A.size; i++)
        {
            int v = A[i];
            size_t j = i;

            while (j >= h && A[j - h] > v)
            {
                A.set(j, A[j - h]);
                j -= h;
            }

            A.set(j, v);
        }
    }

    Globals::finished = true;
}

// heavily adapted from http://www.codecodex.com/wiki/Heapsort

bool isPowerOfTwo(size_t x)
{
    return ((x != 0) && !(x & (x - 1)));
}

uint32_t prevPowerOfTwo(uint32_t x)
{
    x |= x >> 1; x |= x >> 2; x |= x >> 4;
    x |= x >> 8; x |= x >> 16;
    return x - (x >> 1);
}

int largestPowerOfTwoLessThan(int n)
{
    int k = 1;
    while (k < n) k = k << 1;
    return k >> 1;
}

void HeapSort(VisualArray A)
{
    size_t n = A.size, i = n / 2;

    // mark heap levels with different colors
    for (size_t j = i; j < n; ++j)
        A.mark[j] = true;

    while (1)
    {
        if (i > 0) {
            // build heap, sift A[i] down the heap
            i--;
        }
        else {
            // pop largest element from heap: swap front to back, and sift
            // front A[0] down the heap
            n--;
            if (n == 0) break;
            A.swap(0, n);

            A.mark[n] = true;
            if (n + 1 < A.size) A.mark[n + 1] = false;
        }

        size_t parent = i;
        size_t child = i * 2 + 1;

        // sift operation - push the value of A[i] down the heap
        while (child < n)
        {
            if (child + 1 < n && A[child + 1] > A[child]) {
                child++;
            }
            if (A[child] > A[parent]) {
                A.swap(parent, child);
                parent = child;
                child = parent * 2 + 1;
            }
            else {
                break;
            }
        }

        // mark heap levels with different colors
        A.mark[i] = true;
    }

    Globals::finished = true;
}

// by Timo Bingmann

void RadixSortLSD(VisualArray A)
{
    // radix and base calculations
    const unsigned int RADIX = 4;

    int max = A[0];
    for (int i = 0; i < A.size; i++)
        if (A[i] > max)
            max = A[i];

    unsigned int pmax = ceil(log(max + 1) / log(RADIX));

    for (unsigned int p = 0; p < pmax; ++p)
    {
        size_t base = pow(RADIX, p);

        // count digits and copy data
        std::vector<size_t> count(RADIX, 0);
        VisualArray copy;
        copy.init(A.size, END_ENUM, true);
        int idx = Globals::arrays.size();
        Globals::arrays.push_back(copy);

        for (size_t i = 0; i < A.size; ++i)
        {
            copy.set(i, A[i]);
            size_t r = (copy[i]) / base % RADIX;
            assert(r < RADIX);
            count[r]++;
        }

        // exclusive prefix sum
        std::vector<size_t> bkt(RADIX + 1, 0);
        std::partial_sum(count.begin(), count.end(), bkt.begin() + 1);

        // mark bucket boundaries
        for (size_t i = 0; i < bkt.size() - 1; ++i) {
            if (bkt[i] >= A.size) continue;
            A.mark[bkt[i]] = true;
        }

        // redistribute items back into array (stable)
        for (size_t i = 0; i < A.size; ++i)
        {
            size_t r = copy[i] / base % RADIX;
            A.set(bkt[r]++, copy[i]);
        }

        Globals::arrays.erase(Globals::arrays.begin() + idx);
        A.unmarkAll();
    }

    Globals::finished = true;
}

// from http://en.wikipediA.org/wiki/Odd%E2%80%93even_sort
void OddEvenSort(VisualArray A)
{
    bool sorted = false;

    while (!sorted)
    {
        sorted = true;

        for (size_t i = 1; i < A.size - 1; i += 2)
        {
            if (A[i] > A[i + 1])
            {
                A.swap(i, i + 1);
                sorted = false;
            }
        }

        for (size_t i = 0; i < A.size - 1; i += 2)
        {
            if (A[i] > A[i + 1])
            {
                A.swap(i, i + 1);
                sorted = false;
            }
        }
    }

    Globals::finished = true;
}

// ****************************************************************************
// *** Bitonic Sort as "Parallel" Sorting Network

// from http://www.iti.fh-flensburg.de/lang/algorithmen/sortieren/bitonic/oddn.htm

// modified to first record the recursively generated swap sequence, and then
// sort it back into the order a parallel sorting network would perform the
// swaps in

namespace BitonicSortNetworkNS {

    struct swappair_type
    {
        // swapped positions
        unsigned int i, j;

        // depth of recursions: sort / merge
        unsigned int sort_depth, merge_depth;

        swappair_type(unsigned int _i, unsigned int _j,
            unsigned int _sort_depth, unsigned int _merge_depth)
            : i(_i), j(_j),
            sort_depth(_sort_depth), merge_depth(_merge_depth)
        { }

        // order relation for sorting swaps
        bool operator < (const swappair_type& b) const
        {
            if (sort_depth != b.sort_depth)
                return sort_depth > b.sort_depth;

            if (merge_depth != b.merge_depth)
                return merge_depth < b.merge_depth;

            return i < b.i;
        }
    };

    typedef std::vector<swappair_type> sequence_type;
    std::vector<swappair_type> sequence;

    void replay(VisualArray A)
    {
        for (sequence_type::const_iterator si = sequence.begin();
            si != sequence.end(); ++si)
        {
            if (A[si->i] > A[si->j])
                A.swap(si->i, si->j);
        }
    }

    static const bool ASCENDING = true; // sorting direction

    static void compare(VisualArray /* A */, unsigned int i, unsigned int j, bool dir,
        unsigned int sort_depth, unsigned int merge_depth)
    {
        // if (dir == (A[i] > A[j])) A.swap(i, j);

        if (dir)
            sequence.push_back(swappair_type(i, j, sort_depth, merge_depth));
        else
            sequence.push_back(swappair_type(j, i, sort_depth, merge_depth));
    }

    static void bitonicMerge(VisualArray A, unsigned int lo, unsigned int n, bool dir,
        unsigned int sort_depth, unsigned int merge_depth)
    {
        if (n > 1)
        {
            unsigned int m = largestPowerOfTwoLessThan(n);

            for (unsigned int i = lo; i < lo + n - m; i++)
                compare(A, i, i + m, dir, sort_depth, merge_depth);

            bitonicMerge(A, lo, m, dir, sort_depth, merge_depth + 1);
            bitonicMerge(A, lo + m, n - m, dir, sort_depth, merge_depth + 1);
        }
    }

    static void bitonicSort(VisualArray A, unsigned int lo, unsigned int n, bool dir,
        unsigned int sort_depth)
    {
        if (n > 1)
        {
            unsigned int m = n / 2;
            bitonicSort(A, lo, m, !dir, sort_depth + 1);
            bitonicSort(A, lo + m, n - m, dir, sort_depth + 1);
            bitonicMerge(A, lo, n, dir, sort_depth, 0);
        }
    }

    void sort(VisualArray A)
    {
        sequence.clear();
        bitonicSort(A, 0, A.size, BitonicSortNetworkNS::ASCENDING, 0);
        std::sort(sequence.begin(), sequence.end());
        replay(A);
        sequence.clear();
    }

} // namespace BitonicSortNS

void BitonicSortNetwork(VisualArray A)
{
    BitonicSortNetworkNS::sort(A);
    Globals::finished = true;
}