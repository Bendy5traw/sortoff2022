#include "VisualArray.h"

int Globals::numArrays = 0;
std::vector<VisualArray> Globals::arrays = std::vector<VisualArray>();
bool Globals::ready = false;
bool Globals::finished = false;
std::vector<std::shared_ptr<std::thread>> Globals::threads = std::vector<std::shared_ptr<std::thread>>();
int Globals::speed = 2;

int randInt(int min, int max)
{
	return std::rand() % (max + 1 - min) + min;
}

int VisualArray::operator[](int i)
{
	this->access[i] = true;
	this->update();
	return this->array[i];
}

int VisualArray::hiddenAccess(int i)
{
	return this->array[i];
}

void VisualArray::init(int size, Distribution distribution, bool auxArray)
{
	this->array = new int[size];
	this->access = new bool[size];
	this->mark = new bool[size];
	this->arrayNum = Globals::numArrays++;
	this->size = size; // set it now since it's changing during the loop
	this->auxArray = auxArray;

	if (auxArray)
	{
		// init to 0, set to same size as main array to make visualization easier
		for (int i = 0; i < size; i++)
		{
			this->array[i] = 0;
			this->access[i] = false;
			this->mark[i] = false;
		}
	}
	else
	{
		switch (distribution)
		{
		case RANDOM:
			for (int i = 0; i < size; i++)
			{
				int newItem = randInt(1, size);
				this->access[i] = false;
				this->mark[i] = false;

				while (arrayContainsItem(newItem))
					newItem = randInt(1, size);

				this->array[i] = newItem;
			}
			break;
		}
	}
}

// for now, assuming this->array has already been initialized
//int VisualArray::size()
//{
//	return sizeof(this->array) / sizeof(int);
//}

void VisualArray::set(int i, int item)
{
	this->array[i] = item;
	this->access[i] = true;
	this->update();
}

void VisualArray::swap(int i, int j)
{
	int temp = this->array[i];
	this->array[i] = this->array[j];
	this->array[j] = temp;

	this->access[i] = true;
	this->access[j] = true;

	this->update();
}

// wait for ok from visualize function to continue
// if finished, just sleep a static amount instead
void VisualArray::update()
{
	static int clock = 0;
	clock++;
	while (!Globals::ready && !Globals::finished && !(clock % Globals::speed))
		std::this_thread::yield();
	if (Globals::finished)
		std::this_thread::sleep_for(std::chrono::microseconds(50));
	Globals::ready = false;
}

// assuming this->array is initialized for now
int VisualArray::indexOf(int item)
{
	for (int i = 0; i < this->size; i++)
		if (this->array[i] == item)
			return i;
	return -1;
}

// also assuming this->array is initialized for now
bool VisualArray::arrayContainsItem(int item)
{
	for (int i = 0; i < this->size; i++)
		if (this->array[i] == item)
			return true;
	return false;
}

void VisualArray::unmarkAll()
{
	for (int i = 0; i < this->size; i++)
		this->mark[i] = false;
}

VisualArray::~VisualArray()
{
	this->array = NULL;
	this->access = NULL;
	this->mark = NULL;
}
