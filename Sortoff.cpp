#define ARRAY_UTILS
#include <random>
#include <cstdlib>
#include <thread>
#include <numeric>
#include <assert.h>
#include <string>

#include "Sortoff.h"
#include "VisualArray.h"
#include "Algorithms.h"

int main()
{
    std::srand(std::time(NULL));
    // Initialization
    //---------------------------------------------------------------------------------------
    const int screenWidth = 1400;
    const int screenHeight = 800;

    SetConfigFlags(FLAG_WINDOW_UNDECORATED);
    InitWindow(screenWidth, screenHeight, "Sortoff");

    // General variables
    Vector2 mousePosition = { 0 };
    Vector2 windowPosition = { 100, 100 };
    Vector2 panOffset = mousePosition;
    bool dragWindow = false;

    SetWindowPosition(windowPosition.x, windowPosition.y);

    bool exitWindow = false;

    // style here
    GuiLoadStyleDark();

    SetTargetFPS(120);

    // general initializations
    bool generateArray = false;
    bool newThread = true;
    std::thread algo1;

    // GUI initializations
    int sAlgo = 0, eAlgo = 0, dAlgo = 0, cAlgo = 0;
    bool sOpen = false, eOpen = false, dOpen = false, cOpen = false;
    int numCats = 4; // update
    int** cats = new int* [numCats];
    int* catCounts = new int[numCats];
    int selectedCat = -1;
    const char* sAlgoList = "-Simple sorts-;Insertion Sort;Cocktail Shaker Sort;Comb Sort;Shell Sort";
    const char* eAlgoList = "-Efficient sorts-;Merge Sort;Quicksort;Dual-Pivot Quicksort;Heap Sort";
    const char* dAlgoList = "-Distribution sorts-;Radix LSD";
    const char* cAlgoList = "-Concurrent sorts-;Odd-Even Sort;Bitonic Sort";

    int sAlgoCount = 0, eAlgoCount = 0, dAlgoCount = 0, cAlgoCount = 0;
    GuiTextSplit(sAlgoList, ';', &sAlgoCount, NULL);
    GuiTextSplit(eAlgoList, ';', &eAlgoCount, NULL);
    GuiTextSplit(dAlgoList, ';', &dAlgoCount, NULL);
    GuiTextSplit(cAlgoList, ';', &cAlgoCount, NULL);
    catCounts[0] = sAlgoCount, catCounts[1] = eAlgoCount, catCounts[2] = dAlgoCount, catCounts[3] = cAlgoCount;

    // Main game loop
    while (!exitWindow && !WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        mousePosition = GetMousePosition();

        if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
        {
            Rectangle rec;
            rec.x = 0, rec.y = 0, rec.width = screenWidth, rec.height = 20;
            if (CheckCollisionPointRec(mousePosition, rec))
            {
                dragWindow = true;
                panOffset = mousePosition;
            }
        }

        if (dragWindow)
        {
            windowPosition.y += (mousePosition.y - panOffset.y);

            windowPosition.x += (mousePosition.x - panOffset.x);
            if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) dragWindow = false;

            SetWindowPosition(windowPosition.x, windowPosition.y);
        }

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

        ClearBackground(RAYWHITE);

        Rectangle rec;
        rec.x = 0, rec.y = 0, rec.width = screenWidth, rec.height = screenHeight;
        exitWindow = GuiWindowBox(rec, "#198# SORTOFF 2.0");

        Rectangle h2hButton;
        h2hButton.x = screenWidth / 2 - 100, h2hButton.y = 260, h2hButton.width = 200, h2hButton.height = 100;
        Rectangle tournyButton;
        tournyButton.x = screenWidth / 2 - 100, tournyButton.y = 370, tournyButton.width = 200, tournyButton.height = 100;

        static bool showMenu = true;
        static bool auxArray = false;
        windowInfo window;
        window.width = screenWidth, window.height = screenHeight;

        if (showMenu)
        {
            rec.x = screenWidth / 2 - 100, rec.y = 150, rec.width = 200, rec.height = 100;
            DrawText("Sortoff 2.0", screenWidth / 2 - MeasureText("Sortoff 2.0", 50) / 2, 50, 50, RAYWHITE);

            if (GuiButton(rec, "Single Algorithm"))
            {
                buttons::gameType = SINGLE;
                showMenu = false;
                generateArray = true;

            }
            else if (GuiButton(h2hButton, "Head-to-head"))
            {
                buttons::gameType = HEAD_TO_HEAD;
                showMenu = false;
            }
            else if (GuiButton(tournyButton, "Tournament mode"))
            {
                buttons::gameType = TOURNAMENT;
                showMenu = false;
            }
        }
        else if (buttons::gameType == SINGLE)
        {
            DrawText("Select algorithm:", window.width / 2 - MeasureText("Select algorithm:", 40) / 2, 50, 40, RAYWHITE);

            Rectangle dropBox, startButton, nSlider, nSliderLabel, cSliderLabel, cSlider;
            dropBox.x = window.width / 2 - 100, dropBox.y = 150, dropBox.width = 200, dropBox.height = 25;
            startButton.x = window.width / 2 - 100, startButton.y = 260, startButton.width = 200, startButton.height = 100;
            nSliderLabel.x = window.width / 2 - 100, nSliderLabel.y = 360, nSliderLabel.width = 300, nSliderLabel.height = 50;
            nSlider.x = window.width / 2 - 100, nSlider.y = 400, nSlider.width = 200, nSlider.height = 30;
            cSliderLabel.x = window.width / 2 - 100, cSliderLabel.y = 440, cSliderLabel.width = 200, cSliderLabel.height = 50;
            cSlider.x = window.width / 2 - 100, cSlider.y = 480, cSlider.width = 200, cSlider.height = 30;

            int c = 0;

            if (sOpen || eOpen || dOpen || cOpen) GuiLock();

            if (generateArray)
            {
                static float n = 100.f;
                n = GuiSliderBar(nSlider, "10", "1000", n, 10.f, 1000.5f);
                int sz = static_cast<int>(n);
                GuiLabel(nSliderLabel, TextFormat("Number of elements to sort: %i", sz));

                static float speed = 2.f;
                speed = GuiSliderBar(cSlider, "x1", "x10", speed, 1.f, 10.2f);
                int iSpeed = static_cast<int>(speed);
                GuiLabel(cSliderLabel, TextFormat("Speed multiplier: x%i", iSpeed));

                Globals::speed = iSpeed;
                
                if (GuiButton(startButton, "Run algorithm"))
                {
                    generateArray = false;
                    VisualArray array; // remember to destroy when finished using
                    array.init(sz, RANDOM);
                    Globals::arrays.push_back(array);

                    int iAlgo = 0;
                    for (int i = 0; i < selectedCat; i++)
                        iAlgo += catCounts[i] - 1;

                    Algorithm selectedAlgo = (Algorithm)(*cats[selectedCat] + iAlgo);
                    switch (selectedAlgo)
                    {
                    case INSERTION:
                        algo1 = std::thread(insertionSort, array);
                        break;
                    case MERGE:
                        algo1 = std::thread(MergeSort, array);
                        auxArray = true;
                        break;
                    case QUICKSORT:
                        algo1 = std::thread(quickSortLR, array);
                        break;
                    case DP_QUICKSORT:
                        algo1 = std::thread(QuickSortDualPivot, array);
                        break;
                    case COCKTAIL_SHAKER:
                        algo1 = std::thread(CocktailShakerSort, array);
                        break;
                    case COMB_SORT:
                        algo1 = std::thread(CombSort, array);
                        break;
                    case SHELL_SORT:
                        algo1 = std::thread(ShellSort, array);
                        break;
                    case HEAP_SORT:
                        algo1 = std::thread(HeapSort, array);
                        break;
                    case RADIX_LSD:
                        algo1 = std::thread(RadixSortLSD, array);
                        auxArray = true;
                        break;
                    case ODD_EVEN:
                        algo1 = std::thread(OddEvenSort, array);
                        break;
                    case BITONIC:
                        algo1 = std::thread(BitonicSortNetwork, array);
                        break;
                    }

                    buttons::gameType = RUN;
                }

                dropBox.x -= ((numCats * 100) - (numCats * 25));
                if (GuiDropdownBox(dropBox, sAlgoList, &sAlgo, sOpen))
                {
                    sOpen = !sOpen;
                    selectedCat = 0;
                }
                cats[c++] = &sAlgo;

                dropBox.x += (numCats * 100) / 2 + 5;
                if (GuiDropdownBox(dropBox, eAlgoList, &eAlgo, eOpen))
                {
                    eOpen = !eOpen;
                    selectedCat = 1;
                }
                cats[c++] = &eAlgo;

                dropBox.x += (numCats * 100) / 2 + 5;
                if (GuiDropdownBox(dropBox,dAlgoList, &dAlgo, dOpen))
                {
                    dOpen = !dOpen;
                    selectedCat = 2;
                }
                cats[c++] = &dAlgo;

                dropBox.x += (numCats * 100) / 2 + 5;
                if (GuiDropdownBox(dropBox, cAlgoList, &cAlgo, cOpen))
                {
                    cOpen = !cOpen;
                    selectedCat = 3;
                }
                cats[c++] = &cAlgo;


                bool madeSelection = (sAlgo + eAlgo + dAlgo + cAlgo) > 0;
                for (int i = 0; i < numCats; i++) // only allow 1 dropdown selection at a time
                    if (madeSelection && selectedCat != i)
                        *cats[i] = 0;
            }
        }
        else if (buttons::gameType == HEAD_TO_HEAD) // unfinished
        {
            DrawText("Select algorithms:", window.width / 2 - MeasureText("Select algorithms:", 40) / 2, 50, 40, RAYWHITE);

            Rectangle dropBox, dropBox2, startButton, nSlider, nSliderLabel, cSliderLabel, cSlider;
            dropBox.x = window.width / 2 - 100, dropBox.y = 150, dropBox.width = 200, dropBox.height = 25;
            dropBox2.x = dropBox.x, dropBox2.y = dropBox.y + 50, dropBox2.width = dropBox.width, dropBox2.height = 25;
            startButton.x = window.width / 2 - 100, startButton.y = 260, startButton.width = 200, startButton.height = 100;
            nSliderLabel.x = window.width / 2 - 100, nSliderLabel.y = 360, nSliderLabel.width = 300, nSliderLabel.height = 50;
            nSlider.x = window.width / 2 - 100, nSlider.y = 400, nSlider.width = 200, nSlider.height = 30;
            cSliderLabel.x = window.width / 2 - 100, cSliderLabel.y = 440, cSliderLabel.width = 200, cSliderLabel.height = 50;
            cSlider.x = window.width / 2 - 100, cSlider.y = 480, cSlider.width = 200, cSlider.height = 30;

            int c = 0;

            if (sOpen || eOpen || dOpen || cOpen) GuiLock();
        }
        else
        {
            windowInfo window;
            window.width = screenWidth;
            window.height = screenHeight;

            int numArrays = 1; // change this for simultaneous algos running
            if (auxArray)
                numArrays++;
            setupArrayView(numArrays, window);
        }
        GuiUnlock();
        EndDrawing();
    }

    // make algo1 stop yielding and join before closing
    Globals::ready = true;
    algo1.join();

    // De-Initialization
    CloseWindow();        // Close window and OpenGL context

    return 0;
}

// this could use more abstraction
void setupArrayView(int numArrays, windowInfo window)
{
    
    if (Globals::finished)
        validateArray(0, window);
    else
    {
        static int flip = 0;

        while (Globals::ready && !Globals::finished)
            std::this_thread::yield();

        int x = 0, y = 60, height = 600;
        DrawRectangle(0, y, window.width, height, BLACK); // temp, move into loop for multiple arrays. also experiment with side by side format
        int maxHeight = height / Globals::arrays.size(); // split arrays evenly vertically, this method needs to be changed when running multiple algorithms
        for (int i = 0; i < Globals::arrays.size(); i++)
        {
            VisualArray currentArray = Globals::arrays.at(i);
            int scaledHeight = maxHeight * (Globals::arrays.size() - i);
            float scaledWidth = (float)window.width / (float)currentArray.size;

            const char* string = (i % 2) ? "Main array" : "Auxiliary array";
            int sY = (i % 2) ? height + 60 : y - 20; // aux arrays at odd idxs, draw main array at bottom
            if (Globals::arrays.size() > 1) // temp until head to head mode
                DrawText(string, window.width / 2 - (MeasureText(string, 20) / 2), sY, 20, RAYWHITE);

            for (int j = 0; j < currentArray.size; j++)
            {
                Rectangle bar;

                bar.x = (float)j * scaledWidth;
                bar.width = std::fmaxf(scaledWidth - 1.f, 1.f); // draw at least 1 pixel

                if (scaledWidth < 2) // not enough space for 1 pixel gaps, remove the gap
                    bar.x -= 1.f;

                // drawing from top left, maxHeight to appropriate array y start pos
                bar.y = (float)(y + scaledHeight) - ((float)currentArray.hiddenAccess(j) * ((float)(maxHeight) / (float)currentArray.size));

                // probably a better way to do this but it works as intended
                bar.height = std::floor((scaledHeight + y) - bar.y);
                if (bar.y + bar.height >= scaledHeight + y) // prevent rounding to 1 extra pixel
                    bar.height -= 1.f;
                if (bar.y + bar.height < scaledHeight + y - 1.f) // prevent rounding to 1 less pixel
                    bar.height += 1.f;

                Color barColor = (currentArray.access[j]) ? RED : (currentArray.mark[j]) ? SKYBLUE : WHITE;
                GuiDrawRectangle(bar, 0, BLACK, barColor);

                if (!(flip % 3))
                    currentArray.access[j] = false;
            }
        }

        flip++;
    }

    Globals::ready = true;
}

// this is not scaled for multiple arrays and needs to be changed for head to head or tournament
void validateArray(int arrayNum, windowInfo window)
{
    int x = 0, y = 60, height = 600;
    DrawRectangle(0, y, window.width, height, BLACK); // temp, move into loop for multiple arrays. also experiment with side by side format

    VisualArray arr = Globals::arrays.at(arrayNum);
    static bool sorted = true;
    static int step = 1;
    Color bColor;

    // temp fix
    float scaledWidth = (float)window.width / (float)arr.size;

    Rectangle bar;
    for (int i = 0; i < arr.size; i++)
    {
        bColor = (i > 0) ? (i > step) ? WHITE : ((arr.hiddenAccess(i) > arr.hiddenAccess(i - 1)) ? GREEN : RED) : GREEN;
        if (!(arr.hiddenAccess(i) > arr.hiddenAccess(i - 1)))
            sorted = false;

        bar.x = (float)i * scaledWidth;
        bar.width = std::fmaxf(scaledWidth - 1.f, 1.f); // draw at least 1 pixel

        if (scaledWidth < 2) // not enough space for 1 pixel gaps, remove the gap
            bar.x -= 1.f;

        // drawing from top left, maxHeight to appropriate array y start pos
        bar.y = (float)(y + height) - ((float)arr.hiddenAccess(i) * ((float)(height) / (float)arr.size));

        // probably a better way to do this but it works as intended
        bar.height = std::floor((height + y) - bar.y);
        if (bar.y + bar.height >= height + y) // prevent rounding to 1 extra pixel
            bar.height -= 1.f;
        if (bar.y + bar.height < height + y - 1.f) // prevent rounding to 1 less pixel
            bar.height += 1.f;

        GuiDrawRectangle(bar, 0, BLACK, bColor);
    }
    
    step += (arr.size / 100) + 1;
}